import javax.swing.*;

public class Helper {
    public static void SwitchPanel(JPanel destinationPanel) {
        for (JPanel panel: Main.panelList) {
            panel.setVisible(false);
        }
        destinationPanel.setVisible(true);
    }
}
