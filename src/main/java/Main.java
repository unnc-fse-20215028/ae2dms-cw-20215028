
import javax.swing.*;
import java.util.ArrayList;

/**
 * Main creates a JFrame and adds a main.GamePanel to that frame.
 * The size of the GamePanel is determined here.
 */
public class Main {
	public static final int UNIT_SIZE = 20;
	static final int WIDTH = 40;
	static final int HEIGHT = 34;
	public static ArrayList<JPanel> panelList;
	public static int MAX_LEVEL = 2;

	public static void main(String[] args) {
		panelList = new ArrayList<JPanel>();
		JFrame frame = new JFrame();
		frame.setResizable(false);
		MainMenu mainMenu = new MainMenu(WIDTH * UNIT_SIZE, HEIGHT * UNIT_SIZE);
		GameInfo gameInfo = new GameInfo(WIDTH * UNIT_SIZE, HEIGHT * UNIT_SIZE);
		GamePanel gamePanel = new GamePanel(WIDTH * UNIT_SIZE, HEIGHT * UNIT_SIZE);
		panelList.add(mainMenu);
		panelList.add(gameInfo);
		panelList.add(gamePanel);
		mainMenu.setInfoPanel(gameInfo);
		mainMenu.setGamePanel(gamePanel);
		gameInfo.setMainMenuPanel(mainMenu);
		gamePanel.setMainMenu(mainMenu);

		frame.add(gameInfo);
		frame.pack();
		frame.add(mainMenu);
		frame.pack();
		frame.add(gamePanel);
		frame.pack();

		// Set all panel visibility to false except main menu
		for (JPanel panel: Main.panelList) {
			panel.setVisible(false);
		}
		mainMenu.setVisible(true);

		frame.setVisible(true);
		frame.setTitle("Bubble Bobble Game");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
