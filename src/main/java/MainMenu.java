import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainMenu extends JPanel {
    private GameInfo infoPanel;
    private GamePanel gamePanel;

    public MainMenu(int width, int height) {
        JLabel gameTitle = new JLabel("Main Menu");
        JButton startBtn = new JButton("Start Game");
        JButton infoBtn = new JButton("Game Info");
        startBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Helper.SwitchPanel(gamePanel);
                gamePanel.startGame();
            }
        });
        infoBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Helper.SwitchPanel(infoPanel);
            }
        });
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setBorder(new EmptyBorder(new Insets(150, 350, 150, 200)));
        this.add(gameTitle);
        this.add(Box.createRigidArea(new Dimension(0, 50)));
        this.add(startBtn);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(infoBtn);
        this.setPreferredSize(new Dimension(width, height));
    }

    public void setInfoPanel(GameInfo infoPanel) {
        this.infoPanel = infoPanel;
    }

    public void setGamePanel(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
    }
}
