

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * The GamePanel is where the entire game is constantly updated.
 * After every few milliseconds, GamePanel calls the methods that update its InteractableWorld,
 * then repaints the window to display the new drawn graphics.
 */
public class GamePanel extends JPanel {
	InteractableWorld world;
	Timer repaintTimer;
	MainMenu mainMenu;
	JFrame popupFrame;
	int score;

	public GamePanel(int width, int height) {
		world = new InteractableWorld(width, height);
		score = 0;

		this.add(world);

		repaintTimer = new Timer(1000 / 60, new ActionListener() {
			//handles updating the game, going to next frame
			public void actionPerformed(ActionEvent arg0) {
				world.updatePosition();
				if (world.IsGameClear()) {
					score+=50;
					stopGame();
				}
				repaint();
			}
		});
	}

	public void startGame() {
		world.startGame();
		repaintTimer.start();
	}

	public void stopGame() {
		repaintTimer.stop();
		JLabel infoLabel = new JLabel("Congratulations for finishing this level!");
		JLabel scoreLabel = new JLabel("Your Score: " + score);
		JButton closeBtn = new JButton("Main Menu");
		JButton nextLevelBtn = new JButton("Next Level");
		closeBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				popupFrame.dispose();
				Helper.SwitchPanel(mainMenu);
			}
		});
		nextLevelBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				popupFrame.dispose();
				world.NextLevel();
				startGame();
			}
		});
		JPanel popupPanel = new JPanel();
		popupFrame = new JFrame();
		popupPanel.add(infoLabel);
		popupPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		popupPanel.add(scoreLabel);
		popupPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		popupPanel.add(closeBtn);
		popupPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		popupPanel.add(nextLevelBtn);
		popupPanel.setLayout(new BoxLayout(popupPanel, BoxLayout.Y_AXIS));
		popupPanel.setPreferredSize(new Dimension(300,200));
		popupFrame.add(popupPanel);
		popupFrame.pack();
		popupFrame.setResizable(false);
		popupFrame.setVisible(true);

		if (world.GetCurrentLevel() >= Main.MAX_LEVEL) {
			nextLevelBtn.setVisible(false);
		}
	}

	public void setMainMenu(MainMenu mainMenu) {
		this.mainMenu = mainMenu;
	}
}
