import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameInfo extends JPanel {
    private MainMenu mainMenuPanel;

    public GameInfo(int width, int height) {
        //JTextPane gameInfo = new JTextPane();
        JTextArea gameInfo = new JTextArea();
        gameInfo.setLineWrap(true);
        gameInfo.setWrapStyleWord(true);
        gameInfo.setPreferredSize(new Dimension(width, height/2));
        gameInfo.setText("##### Gameplay #####\n" +
                "You play as a red square that can move around, jump, and use a few tools to defend against the enemy blue and green squares. The objective of the game is to defeat all the enemies in the level by bubbling them and subsequently popping them. If you come into contact with an enemy (or the bullets they shoot), you lose a life and the level is restarted. If you lose all of your lives, you return to the first level and your score is reset to 0.\n" +
                "\n" +
                "The tools you can use against enemies are:\n" +
                "- **main.Bubble:** Your main weapon. It encases all enemies it comes into contact with into bubbles, making them float upwards and become vulnerable to popping. However, enemies will break out of their bubbles and become dangerous again if they are not popped in time.\n" +
                "- **Shield:** A defensive aura that surrounds you. Any enemy that comes into contact with the shield are repelled, and you don't lose any lives. If you activate the shield for too long, the shield will break, leaving you paralyzed and vulnerable to enemy attacks.\n" +
                "- **Bomb:** Your strongest weapon. When used, every enemy in the level is bubbled, leaving you free to pop them. It can only be used once the bar on the right side of the screen, which fills up over time, is fully charged.\n" +
                "\n\n##### Controls #####\n" +
                "- **Left and right arrow keys:** Move in a horizontal direction.\n" +
                "- **Up arrow key:** Jump upwards.\n" +
                "- **Space:** When held, you move faster than normal, as if you were dashing.\n" +
                "- **E:** Shoot a bubble.\n" +
                "- **Q:** Activate your shield.\n" +
                "- **W:** Detonate a bomb.");
        JButton backBtn = new JButton("Back");
        backBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Helper.SwitchPanel(mainMenuPanel);
            }
        });
        this.add(gameInfo);
        this.add(backBtn);
        this.setPreferredSize(new Dimension(width, height));
    }

    public void setMainMenuPanel(MainMenu panel) {
        mainMenuPanel = panel;
    }
}
